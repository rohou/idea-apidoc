package com.jlh.com.jlh;

import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiAnnotationMemberValue;

import java.util.Optional;
import java.util.stream.Stream;

public enum MVCMethodEnums {
    GET("GetMapping"),
    POST("PostMapping"),
    PUT("PutMapping"),
    DELETE("DeleteMapping"),
    RequestMapping("RequestMapping"),;

    private String name;

    public String httpType(PsiAnnotation annotation){
        if (this.equals(MVCMethodEnums.RequestMapping)){
            PsiAnnotationMemberValue method = annotation.findDeclaredAttributeValue("method");
            if (method == null){
                return MVCMethodEnums.POST.toString();
            }else {
                return method.getText().replace("RequestMethod.", "");
            }
        }else {
            return this.toString();
        }

    }


    public static MVCMethodEnums findByName(String label){

        Optional<MVCMethodEnums> first = Stream.of(MVCMethodEnums.values())
                .filter(m -> label.contains(m.getName()))
                .findFirst();

        if (!first.isPresent()){
            throw new RuntimeException("MVCMethodEnums not include "+label);
        }

        return first.get();

    }


    MVCMethodEnums(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
